@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">

            <div class="card-body">
                <form method="POST" action="{{route('applicants.store')}}">
                    @csrf

                    <div class="form-group row">
                        <label for="site" class="col-md-4 col-form-label text-md-right">{{ __('Surname') }}</label>

                        <div class="col-md-6">

                            <input id="surname" type="text" class="form-control "
                                   name="surname" value="" required autofocus>

                        </div>




                    </div>

                    <div class="form-group row">
                        <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('First Name') }}</label>

                        <div class="col-md-6">

                            <input id="first_name" type="text" class="form-control "
                                   name="firstname" value="" required autofocus>


                        </div>


                    </div>

                    <div class="form-group row">
                        <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Other Name') }}</label>

                        <div class="col-md-6">


                            <input id="other_name" type="text" class="form-control "
                                   name="othername" value="" required autofocus>


                        </div>


                    </div>

                    <div class="form-group row">
                        <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Date Of Birth') }}</label>

                        <div class="col-md-6">


                            <input id="dob" type="date" class="form-control "
                                   name="dob" value="" required autofocus>

                        </div>


                    </div>

                    <div class="form-group row">
                        <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Contact') }}</label>

                        <div class="col-md-6">


                            <input id="contact" type="text" class="form-control "
                                   name="contact" value="" required autofocus>

                        </div>


                    </div>

                    <div class="form-group row">
                        <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Residence') }}</label>

                        <div class="col-md-6">


                            <input id="residence" type="text" class="form-control "
                                   name="residence" value="" required autofocus>

                        </div>


                    </div>

                    <div class="form-group row">
                        <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Postal Address') }}</label>

                        <div class="col-md-6">


                            <input id="postal_address" type="text" class="form-control "
                                   name="postal_address" value="" required autofocus>



                        </div>



                    </div>





                    <div class="form-group row mb-0">
                        <div class="col-md-6 offset-md-4">
                            <button type="submit" class="btn btn-primary">
                                {{ __('Save') }}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

