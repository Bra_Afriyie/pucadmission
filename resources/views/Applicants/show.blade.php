@extends('layouts.app')

@section('content')

    <div class="col-md-6 col-lg-6 col-md-offset-3 col-lg-offset-3">

        <form method="POST"  action="{{ URL::to('SendSMS') }}/{{$applicant->id }}">
            @csrf


            <div class="form-group row">
                <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Serial Number') }}</label>

                <div class="col-md-6">

                    <h1 class="display-3">{{$applicant->surname}}</h1>



                    <h1 name="serialnumber">
                    Serial Number
                    </h1>

                    <h1 name="serialnumber">
                        {{$applicant->serialnumber}}
                    </h1>



                </div>


            </div>


            <div class="form-group row mb-0">
                <div class="col-md-6 offset-md-4">
                    <button type="submit" class="btn btn-success">
                        {{ __('Send Code') }}
                    </button>
                </div>
            </div>
        </form>



    </div>

@endsection
