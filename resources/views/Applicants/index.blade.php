@extends('layouts.app')

@section('content')

    <div class="col-md-12 col-lg-12 col-md-offset-3 col-lg-offset-3">


        <div class="card card-primary" >



            <div class="card-body">




                <table class="table table-bordered">
                    <thead>
                    <th>Surname</th>
                    <th>FirstName</th>
                    <th>OtherName</th>
                    <th>Contact</th>
                    <th>Serial Number</th>
                    <th>Pin Code</th>
                    </thead>
                    <tbody>
                    @foreach($applicants as $applicant)
                        <tr>
                            <td>{{$applicant->surname}}</td>
                            <td>{{$applicant->first_name}}</td>
                            <td>{{$applicant->other_name}}</td>
                            <td>{{$applicant->contact}}</td>
                            <td>{{$applicant->serialnumber}}</td>
                            <td>{{$applicant->pincode}}</td>
                            <td>

                                <a href="/applicants/{{$applicant->id}}" class="btn btn-link my-1"><i class="fa fa-pencil"></i>Send PinCode</a>
                                <a href="" class="btn btn-success my-1"><i class="fa fa-pencil"></i>Edit</a>
                                <a href="" class="btn btn-danger my-1"><i class="fa fa-trash"></i>Delete</a>



                                {{--       <a href="{{URL::to('department/'.$department->department_id)}}" class="btn btn-success my-1"><i class="fa fa-pencil"></i>Edit</a>
                                       <a href="{{URL::to('department/'.$department->department_id.'/delete')}}" class="btn btn-danger my-1"><i class="fa fa-trash"></i>Delete</a>--}}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>



            </div>



        </div>


    </div>

@endsection
