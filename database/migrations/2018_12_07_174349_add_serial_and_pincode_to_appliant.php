<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSerialAndPincodeToAppliant extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('applicants', function($table) {
            $table->string('serialnumber');
            $table->integer('pincode')->nullable();;
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //

        Schema::table('applicants', function($table) {
            $table->dropColumn('serialnumber');
            $table->dropColumn('pincode');
        });
    }
}
