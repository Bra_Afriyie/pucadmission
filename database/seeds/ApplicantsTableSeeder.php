<?php


use Illuminate\Database\Seeder;

class ApplicantsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $applicant = new App\Models\Applicant();
        $applicant->surname = "Linda";
        $applicant->first_name = "Owusuaa";
        $applicant->other_name = "Afriyie";
        $applicant->dob = "2010-10-10";
        $applicant->contact = "0240857548";
        $applicant->postal_address = "Box 20";
        $applicant->residence = "Kumasi";
        $applicant->user_id = "1";
        $applicant->save();
    }
}
