<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Applicant extends Model
{
    //

    protected $fillable =[
        'surname',
        'first_name',
        'other_name',
        'dob',
        'contact',
        'postal_address',
        'residence',
        'serialnumber',
        'pincode',
        'user_id',
    ];

   /* public function ApplicantAuth()
    {
        return $this->belongsTo('App\Models\ApplicantAuth');
    }*/


    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
