<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SendSMS extends Model
{
    //

/*


    private $_endpoint;
    public $key;
    public $message;
    public $numbers;
    public $sender;

      $this->_endpoint = "https://apps.mnotify.net/smsapi";*/



    public function sendMessage()
    {
        /*$url = $this->_endpoint . "?key=" . $this->key . "&to=" . $this->numbers . "&msg=" . $this->message . "&sender_id=" . $this->sender;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        $result = curl_exec($ch);
        curl_close ($ch);*/

        //defining the parameters
        $key = "44d7a5066d1910a17515";  // Remember to put your own API Key here
        $to = "0240857548";
        $msg = "Sending SMS has never been this fun!";
        $sender_id = "PUC"; //11 Characters maximum
        $date_time = "2017-05-02 00:59:00";

//encode the message
        $msg = urlencode($msg);

//prepare your url
        $url = "https://apps.mnotify.net/smsapi?"
            . "key=$key"
            . "&to=$to"
            . "&msg=$msg"
            . "&sender_id=$sender_id"
            . "&date_time=$date_time";
        $response = file_get_contents($url) ;
//response contains the response from mNotify

       // NB: Remember that this is a GET request


        return $this->interpret($response);
    }

    private function interpret($code)
    {
        $status = '';
        switch ($code) {
            case '1000':
                $status = 'Messages has been sent successfully';
                return $status;
                break;
            case '1002':
                $status = 'SMS sending failed. Might be due to server error or other reason';
                return $status;
                break;
            case '1003':
                $status = 'Insufficient SMS credit balance';
                return $status;
                break;
            case '1004':
                $status = 'Invalid API Key';
                return $status;
                break;
            case '1005':
                $status = 'Invalid recipient\'s phone number';
                return $status;
                break;
            case '1006':
                $status = 'Invalid sender id. Sender id must not be more than 11 characters. Characters include white space';
                return $status;
                break;
            case '1007':
                $status = 'Message scheduled for later delivery';
                return $status;
                break;
            case '1008':
                $status = 'Empty Message';
                return $status;
                break;
            default:
                return $status;
                break;
        }
    }


}
