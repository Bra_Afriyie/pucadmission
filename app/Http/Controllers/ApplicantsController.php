<?php

namespace App\Http\Controllers;

use App\Models\Applicant;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ApplicantsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        if(Auth::check())
        {

            $applicants = Applicant::all();

            return view('applicants.index',['applicants' => $applicants]);


        }

        return view('auth.login');


       }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

        if(Auth::check())
        {


            return view('Applicants.create');

        }

        return view('auth.login');


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        if(Auth::check())
        {


            //Generate Random Number
            do{

               // echo rand(1000000,90000000);
               // $randomString = str_random(2).rand().str_random(2);

                $serialnumber =  rand(1000000,90000000);
                $rand_serial =  "PUC".$serialnumber;

                $pincode =  rand(1000,9000);
                $rand_pin =  $pincode;
            }while(!empty(Applicant ::where('serialnumber',$rand_serial)->first()));




          /*  $Clients = new clients ;
            $Clients ->name = $request->name;
            $Clients ->client_number = $rand;
            $Clients ->save();*/

            $applicant= Applicant::create([
                'surname' => $request->input('surname'),
                'first_name' => $request->input('firstname'),
                'other_name' => $request->input('othername'),
                'dob' => $request->input('dob'),
                'contact' => $request->input('contact'),
                'postal_address' => $request->input('postal_address'),
                'residence' => $request->input('residence'),
                    'serialnumber' => $rand_serial,
                    'pincode' => $rand_pin,
                'user_id' => Auth::user()->id

            ]);


            if($applicant)
            {
              // return redirect()->route('applicants.show',['company'=>$applicant->id])

                return redirect()->route('applicants.index')
                 ->with('success','Applicants created successfully');
            }
        }

        return back()->withInput()->with('errors','Error creating new Applicants');



    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Applicant  $applicant
     * @return \Illuminate\Http\Response
     */
 /*   public function show(Applicant $applicant)
    {
        //

       // $applicant = Applicant::where('id',$applicant);

        if(Auth::check())
        {


            $applicantid = $applicant::find($applicant->id);

          return view('Applicants.show',['applicant'=>$applicantid]);

        }

        return view('auth.login');

    }*/

    public function show(Applicant $applicant)
    {
        //

        if(Auth::check())
        {


            $applicant = $applicant::find($applicant->id);

            echo ($applicant->id);



            return view('Applicants.show',['applicant'=>$applicant]);

        }

        return view('auth.login');

    }














    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Applicant  $applicant
     * @return \Illuminate\Http\Response
     */
    public function edit(Applicant $applicant)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Applicant  $applicant
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Applicant $applicant)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Applicant  $applicant
     * @return \Illuminate\Http\Response
     */
    public function destroy(Applicant $applicant)
    {
        //
    }


}
