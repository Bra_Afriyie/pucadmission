<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::middleware(['auth'])->group(function (){


    Route::resource('applicants','ApplicantsController');

   // Route::get ('admission','AdmissionController');

    Route::get('/admission/login', 'HomeController@login')->name('login');



     Route::post('/SendSMS/{id}', 'SendSMSController@sendMessage')->name('sendMessage');
   // Route::post('/admission/{$applicantid}', 'AdmissionController@CheckCredentials')->name('CheckCredentials');
    Route::post('/admission/{$applicantid}', 'SendSMSController@CheckCredentials')->name('CheckCredentials');


});
